let num = 5;
let getCube = num** 3;
console.log(`The cube of ${num} is ${getCube}`);

let address = ["Karangalan","Makati"];
let [brgy,city] = address;

console.log(`Full address: Brgy. ${brgy}, ${city} City`);
let animal = {name:"berto",type:"dog",breed:"pinoy"}
let {name,type,breed} = animal;
console.log(`This animal is named ${name} and he is a ${breed} ${type}`);

let numbers = [1,3,4,56,64];
numbers.forEach(x=>console.log(x));

let reduceNumber = (x,y)=> x+y;
console.log(`The sum of all numbers on the array is: ${numbers.reduce(reduceNumber)}`);

class Dog{
	constructor(dogName,dogAge,dogBreed){
		this.dogName = dogName;
		this.dogAge = dogAge;
		this.dogBreed = dogBreed;
	}
}

let bullDog = new Dog("Iggy",5,"Bull Dog");
console.log(bullDog);